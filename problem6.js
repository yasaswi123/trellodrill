//Create a new board, create 3 lists simultaneously, and a card in each list simultaneously
const createdBoard = require('./problem2')

function newBoard(boardName) {
    return new Promise((resolve, reject) => {
        createdBoard(boardName)
            .then((result) => {
                return Promise.all([
                    createList('list1', result.id),
                    createList('list2', result.id),
                    createList('list3', result.id)
                ])
            })
            .then(lists => {
                return Promise.all(lists.map(list => createCards(list.id, `Card in ${list.name}`)));
            })
            .then(cards => {
                resolve(cards)
                console.log('Cards created:', cards);
            })
            .catch(err => reject(err));
    })
}

newBoard('board2')
    .then((result) => {
        console.log(result)
    })
    .catch((err) => console.error(err))


function createList(name, boardId) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists?name=${name}&idBoard=${boardId}&key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                resolve(data)
            })
            .catch((err) => reject(err))
    })
}
function createCards(listId, name) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/cards?idList=${listId}&name=${name}&key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                resolve(data)
            })
            .catch((err) => reject(err))
    })
}
module.exports = newBoard


