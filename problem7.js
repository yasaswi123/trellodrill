//Delete all the lists created in Step 6 simultaneously
const create = require('./problem6')

function creating() {
    create("board2")
        .then((cards) => {
            let deletePromises = [];
            for (let card of cards) {
                let deleteEachPromise = deleteList(card.idList);
                deletePromises.push(deleteEachPromise);
            }

            return Promise.all(deletePromises);
        })
        .then((data) => {
            console.log(data);
        })
        .catch((err) => {
            console.error(err);
        });
}

function deleteList(listID) {

    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists/${listID}/closed?value=true&key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`, {
            method: "PUT",
        })
            .then(response => response.json())
            .then((cardDelete) => {
                resolve(cardDelete);
            }).catch((err) => {
                reject(err);
            });
    });
}
creating()