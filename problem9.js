//Update all checkitems in a checklist to completed status simultaneously
const fetch = require('node-fetch')

function checkItems(checklistId) {
    return fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`)
        .then(response => response.json())
        .catch(err => {
            console.err(err);
        });
}

function checkComplete(cardId, checkitemId) {
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}?state=complete&key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`;
    return fetch(url, {
        method: 'PUT'
    })
        .then(response => response.json())
        .catch(err => {
            console.error( err);
        });
}

function checkItemsCompleted(checklistId, cardId) {
    return checkItems(checklistId)
        .then(checkitems => {
            const updated = checkitems.map(checkitem =>
                checkComplete(cardId, checkitem.id)
            );
            return Promise.all(updated);
        })
        .then(() => {
            console.log('check items are updated');
        })
        .catch(err => {
            console.error(err);
        });
}

const checklistId = '665505d15090d247be218ea7';
const cardId = '665504d5055c2a1b759d265d';
checkItemsCompleted(checklistId, cardId);