//Create a function getLists which takes a boardId as argument and returns a promise which resolved with lists data

const fetch = require('node-fetch')

function problem3(id) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/${id}/lists?key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then((lists) => {
                resolve(lists.json())
            })
            .catch((err)=>reject(err))
    })
}
problem3('6654124bce391cfdc638344e')
    .then((result) => {
        console.log(result)
    })
    .catch((err) => console.error(err))

module.exports = problem3