//Create a function getAllCards which takes a boardId as argument and which uses getCards function to fetch cards of all the lists. Do note that the cards should be fetched simultaneously from all the lists.
const getCards = require('./problem4')
const getLists = require('./problem3')

function problem5(boardId) {
    return new Promise((resolve, reject) => {
        getLists(boardId)
            .then((allLists) => {
                const lists = allLists.map(listObject => getCards(listObject.id))
                Promise.all(lists)
                    .then(result => {
                        resolve(result)
                    })
                    .catch((err)=>reject(err))
            })
    })
}
problem5('6654124bce391cfdc638344e')
    .then((result) => {
        console.log(result)
    })
    .catch((err) => {
        console.error(err)
    })