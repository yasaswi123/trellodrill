const fetch = require('node-fetch');

function getChecklistIDs(cardID) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/cards/${cardID}/checklists?key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        })
        .then(response => {
           return response.json();
        })
        .then(checklists => {
            const checklistIDs = checklists.map(checklist => checklist.id);
            resolve(checklistIDs);
        })
        .catch(err => reject(err));
    });
}

// Usage example
const cardID = '665504d5055c2a1b759d265d';
getChecklistIDs(cardID)
    .then(checklistIDs => console.log('Checklist IDs:', checklistIDs))
    .catch(err => console.error('Error:', err));
