const fetch = require('node-fetch')

function problem2(name) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/?name=${name}&key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`, {
            method: 'POST',
        })
            .then((createboard) => {
                resolve(createboard.json())
            })
            .catch((err)=>reject(err))
    })
}
problem2("board1")
    .then((result) => {
        console.log(result)
    })
    .catch(err => console.error(err))

module.exports = problem2

