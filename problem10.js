//Update all checkitems in a checklist to incomplete status sequentially i.e. Item 1 should be updated -> then wait for 1 second -> then Item 2 should be updated etc.
const fetch = require('node-fetch')

function checkItems(checklistId) {
    const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`;
    return fetch(url)
        .then(response => response.json())
        .catch(err => {
            console.error(err);
        });
}

function updateIncomplete(cardId, checkitemId) {
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}?state=incomplete&key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`;
    return fetch(url, {
        method: 'PUT'
    })
        .then(response => response.json())
        .catch(err => {
            console.error(err);
        });
}

function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

function updateSequential(checklistId, cardId) {
    return checkItems(checklistId)
        .then(checkitems => {
            const updatePromises = checkitems.map(checkitem => {
                return () => updateIncomplete(cardId, checkitem.id)
                    .then(() => delay(1000));
            });
            return updatePromises.reduce((chain, update) => {
                return chain.then(update);
            }, Promise.resolve());
        })
        .then(() => {
            console.log('Checkitems marked incomplete');
        })
        .catch(err => {
            console.error(err);
        });
}

const checklistId = '665505d15090d247be218ea7';
const cardId = '665504d5055c2a1b759d265d';
updateSequential(checklistId, cardId);