//Create a function getCards which takes a listId as argument and returns a promise which resolves with cards data

const fetch = require('node-fetch')
function problem4(listid) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists/${listid}/cards?key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`, {
            method: 'GET'
        })
            .then((cards) => {
                resolve(cards.json())
            })
    })
}
problem4('6654124bceb8b2de281a0348')
    .then((result) => {
        console.log(result)
    })
    .catch((err) => console.error(err))

module.exports=problem4