//Delete all the lists created in Step 6 sequentially i.e. List 1 should be deleted -> then List 2 should be deleted etc.
const create = require('./problem6')

function creating() {
    return new Promise((resolve, reject) => {
        create('board2').then((data) => {
            for (let list of data) {
                deleteList(list.idList)
            }
        }).then(resolve('deleteLists'))
    })
}

function deleteList(listID) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists/${listID}/closed?value=true&key=fe8955870c615c9c659c60966abd5588&token=ATTAa05fb80803096296c1a0b5cbe580dc39fbb6dc777a6d92a8aeed8659c83e89060D67BAD5`, {
            method: "PUT",
        })
            .then(response => response.json())
            .then((cardDelete) => {
                resolve(cardDelete);
            }).catch((err) => {
                reject(err);
            });
    });
}
creating()
    .then((result) => {
        console.log(result)
    })